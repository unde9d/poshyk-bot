import winston, { format, transports } from "winston";

export const logger = winston.createLogger({
  format: format.combine(
    format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    format.errors({ stack: true }),
    format.json()
  ),
  transports: [new transports.Console()],
});
