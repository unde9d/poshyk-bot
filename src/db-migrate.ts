import { setupSlonikMigrator } from "@slonik/migrator";
import path from "path";
import { pool } from "./db";

setupSlonikMigrator({
  slonik: pool,
  migrationsPath: path.join(__dirname, "../migrations"),
  mainModule: module,
});
