import bent from "bent";
import { JSDOM } from "jsdom";
import { logger } from "./logger";

export type Post = {
  id: string;
  url: string;
  message: string;
};

const baseTelegramWebLink = "https://t.me/";

export async function loadLatestPosts() {
  const html = await fetchPage().catch((e) => {
    logger.error({
      message: "Error loading posts",
      error: e,
    });
    return "";
  });
  const page = new JSDOM(html);

  const postDivs = page.window.document.querySelectorAll("div[data-post]");

  const posts: Post[] = [];

  postDivs.forEach((postDiv) => {
    const postUrlPath = (postDiv as any).dataset.post;
    const postUrl = `${baseTelegramWebLink}${postUrlPath}`;
    const postId = postUrlPath.split("/")[1];
    const messages: string[] = [];
    postDiv.querySelectorAll(".tgme_widget_message_text").forEach((textDiv) => {
      messages.push(textDiv.textContent || "");
    });
    const post = {
      id: postId,
      url: postUrl,
      message: messages.join("\n"),
    };

    posts.push(post);
  });

  return posts;
}

export function subscribeToPosts(fn: (posts: Post[]) => void) {
  let lastPostId = "";
  const timerId = setInterval(async function () {
    const posts = await loadLatestPosts();
    const newPosts = posts.filter((post) => post.id > lastPostId);
    logger.info({
      message: "New posts",
      newPosts: newPosts.length,
    });
    if (!newPosts.length) return;
    lastPostId = newPosts[newPosts.length - 1].id;
    fn(newPosts);
  }, 10000);

  return () => clearInterval(timerId);
}

const fetchPage = async () => {
  const request = bent(baseTelegramWebLink);

  const responseStream = (await request("s/spiski_okrestina")) as {
    text: () => Promise<string>;
  };

  return responseStream.text();
};
