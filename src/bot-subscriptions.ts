import type { CallbackQuery, Message } from "node-telegram-bot-api";
import { bot } from "./bot";
import type { Post } from "./channel-posts";
import { logger } from "./logger";
import { findByLastname } from "./spiski-live";
import {
  addSubscription,
  getSubscriptions,
  getSubscriptionsByChatId,
  removeSubscription,
  removeSubscriptions,
} from "./subscriptions";

export const routeMessage = async (message: Message) => {
  const text = message.text || "";
  const chatId = message.chat.id;
  if (/^\/start/.test(text)) {
    return welcomeAction(chatId).catch((e) => {
      logger.error({
        message: "Error: Welcome action",
        error: e,
      });
    });
  }
  return searchAction(chatId, text).catch((e) => {
    bot.sendMessage(chatId, "Упс, произошла ошибка");
    logger.error({
      message: "Error: subscribe action",
      error: e.toString(),
    });
  });
};

export const sendNewPostsToSubscribers = async (newPosts: Post[]) => {
  const subscriptions = await getSubscriptions();
  newPosts.forEach(async (post) => {
    for (let subscription of subscriptions) {
      const [chatId, lastNames] = subscription;
      lastNames.forEach((lastName) => {
        if (post.message.toLowerCase().indexOf(lastName.toLowerCase()) > -1) {
          bot.sendMessage(
            chatId,
            `Новая информация про ${lastName}\n${post.url}`
          );
          logger.info({
            message: "Update was sent",
            chatId,
          });
        }
      });
    }
  });
};

const subscribeCommand = /^subscribe_to (.*)$/;
const unsubscribeCommand = /^unsubscribe_to (.*)$/;
const newsearchCommand = /^new_search$/;
export const handleCallbackQuery = async (callbackMessage: CallbackQuery) => {
  if (!callbackMessage.data) return;
  const subscribeToCommandParams = subscribeCommand.exec(callbackMessage.data);
  const unsubscribeToCommandParams = unsubscribeCommand.exec(
    callbackMessage.data
  );
  const searchParams = newsearchCommand.exec(callbackMessage.data);
  const chatId = callbackMessage.from.id;
  if (subscribeToCommandParams) {
    const [, lastName] = subscribeToCommandParams;
    await subscribeToLastNameAction(chatId, lastName);
  }
  if (unsubscribeToCommandParams) {
    const [, lastName] = unsubscribeToCommandParams;
    await unsubscribeFromLastNameAction(chatId, lastName);
  }
  if (searchParams) {
    await newSearchMessage(chatId);
  }
  bot.answerCallbackQuery(callbackMessage.id);
};

export const handleInlineQuery = () => {};

const instruction = `Отправьте фамилию, чтобы получить доступную информацию и подписаться на обновления.`;
const welcomeMessage = `Привет! Я помогаю искать задержанных. ${instruction}`;
async function welcomeAction(chatId: number) {
  logger.info({
    message: "Welcome",
    chatId,
  });
  await removeSubscriptions(chatId);
  return bot.sendMessage(chatId, welcomeMessage);
}

async function searchAction(chatId: number, lastName: string) {
  logger.info({
    message: "Searching by last name",
    lastName,
  });
  return sendInformationFromSpiski(chatId, lastName);
}

async function subscribeToLastNameAction(chatId: number, lastName: string) {
  logger.info({
    message: "Subscribing to last name",
    lastName,
  });
  await addSubscription(chatId, lastName);
  return sendInformationFromSpiski(chatId, lastName);
}

async function unsubscribeFromLastNameAction(chatId: number, lastName: string) {
  logger.info({
    message: "Unsubscribing from last name",
    lastName,
  });
  await removeSubscription(chatId, lastName);
  return sendInformationFromSpiski(chatId, lastName);
}

async function newSearchMessage(chatId: number) {
  bot.sendMessage(chatId, instruction);
}

async function sendInformationFromSpiski(chatId: number, lastName: string) {
  const existingSubscriptions = await getSubscriptionsByChatId(chatId);
  const actionButton =
    existingSubscriptions &&
    existingSubscriptions.subscriptions.find((s) => s === lastName)
      ? {
          text: "Отписаться от новой информации об этой фамилии",
          callback_data: `unsubscribe_to ${lastName}`,
        }
      : {
          text: "Подписаться на новую информацию об этой фамилии",
          callback_data: `subscribe_to ${lastName}`,
        };
  const messages = await findByLastname(lastName);
  for (let availableInfo of messages.slice(0, -1)) {
    await bot.sendMessage(chatId, availableInfo);
  }
  await bot.sendMessage(chatId, messages[messages.length - 1], {
    reply_markup: {
      inline_keyboard: [
        [actionButton],
        [
          {
            text: "Сообщить новую информацию",
            url:
              "https://docs.google.com/forms/d/e/1FAIpQLScpSOicLFqYdzJO3UeHtnFJy8RWdDM0YvHVev_tNZdXqIuhNQ/viewform",
          },
        ],
        [
          {
            text: "Открыть на spiski.live",
            url: `http://spiski.live?q=${lastName}`,
          },
        ],
        [
          {
            text: "Искать другую фамилию",
            callback_data: "new_search",
          },
        ],
      ],
    },
  });
}
