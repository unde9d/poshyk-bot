import { sql } from "slonik";
import { pool } from "./db";
import {
  addSubscription,
  getSubscriptions,
  getSubscriptionsByChatId,
  removeSubscription,
} from "./subscriptions";

beforeEach(() => pool.query(sql`TRUNCATE subscriptions`));

it("adds subscription to new table", async () => {
  await addSubscription(10, "Test 1");
  const allSubscriptions = await getSubscriptions();
  expect(allSubscriptions[0][0]).toBe(10);
  expect(allSubscriptions[0][1]).toEqual(["Test 1"]);
});

it("updates subscription when chat_id exists", async () => {
  await addSubscription(10, "Test 1");
  await addSubscription(10, "Test 2");
  const allSubscriptions = await getSubscriptions();
  expect(allSubscriptions[0][0]).toBe(10);
  expect(allSubscriptions[0][1]).toEqual(["Test 1", "Test 2"]);
});

it("removes subscription when there is occurence", async () => {
  await addSubscription(11, "Test1");
  await removeSubscription(11, "Test1");
  const subscriptions = await getSubscriptionsByChatId(11);
  expect(subscriptions).toBeNull();
});
