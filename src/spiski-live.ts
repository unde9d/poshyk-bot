import bent from "bent";
import { logger } from "./logger";

const noInfo = `Информации о задержании такого человека нет. Если вы уверены, что это не так, напишите ФИО и дату задержания в Телеграме в
@spiskov_net_bot`;

export async function findByLastname(lastName: string) {
  const response = await fetchPage(lastName).catch((e) => {
    logger.error({
      message: "Error spiski.live fetch",
      error: e,
    });
    return [noInfo];
  });
  if (!Array.isArray(response)) {
    return [noInfo];
  }
  if (!response || (Array.isArray(response) && !response.length)) {
    return [noInfo];
  }

  const sections = response.map((section) =>
    Object.keys(section)
      .filter((key) => !!section[key])
      .map((key) => `${key}: ${section[key]}`)
      .join("\n")
  );

  return toMessages(sections);
}

const messageLengthLimit = 4096;
const toMessages = (sections: string[]) => {
  const messages = [];
  let currentMessage = "";
  for (let section of sections) {
    if (currentMessage.length + section.length < messageLengthLimit - 1) {
      currentMessage += "\n" + section;
    } else {
      messages.push(currentMessage);
      currentMessage = "";
    }
  }
  if (currentMessage) {
    messages.push(currentMessage);
  }
  return messages;
};

const fetchPage = async (lastName: string) => {
  const request = bent("http://spiski.live/");

  const responseStream = (await request(`api?q=${lastName}`)) as {
    json: () => Promise<any>;
  };

  const response = await responseStream.json();
  return response && response[lastName];
};
