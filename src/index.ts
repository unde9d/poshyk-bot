import { bot } from "./bot";
import { subscribeToPosts } from "./channel-posts";
import { logger } from "./logger";
import {
  handleCallbackQuery,
  handleInlineQuery,
  routeMessage,
  sendNewPostsToSubscribers,
} from "./bot-subscriptions";

logger.info({
  message: "App started",
});

bot.on("message", routeMessage);
bot.on("callback_query", handleCallbackQuery);
bot.on("inline_query", handleInlineQuery);

subscribeToPosts(sendNewPostsToSubscribers);
