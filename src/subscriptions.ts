import { sql } from "slonik";
import { pool } from "./db";

export type ChatId = number;
export type Subscriptions = string[];

export async function addSubscription(chatId: ChatId, lastName: string) {
  await pool.query(sql`
    INSERT INTO subscriptions(chat_id, subscriptions)
    VALUES (${chatId}, ${sql.json([lastName])})
    ON CONFLICT (chat_id)
    DO
      UPDATE SET subscriptions = subscriptions.subscriptions || EXCLUDED.subscriptions
  `);
}

export async function getSubscriptions(): Promise<
  Array<[ChatId, Subscriptions]>
> {
  const allRows = await pool.any(
    sql`SELECT chat_id, subscriptions FROM subscriptions`
  );
  return allRows.map((row) => [
    row.chat_id as number,
    row.subscriptions as string[],
  ]);
}

export async function getSubscriptionsByChatId(chatId: number) {
  const row = await pool.maybeOne(
    sql`SELECT chat_id, subscriptions FROM subscriptions WHERE chat_id=${chatId}`
  );
  if (!row) return row;
  return {
    chat_id: row.chat_id as number,
    subscriptions: row.subscriptions as string[],
  };
}

export async function removeSubscriptions(chatId: ChatId) {
  await pool.query(sql`
    DELETE FROM subscriptions s WHERE s.chat_id = ${chatId}
  `);
}

export async function removeSubscription(chatId: ChatId, lastName: string) {
  const s = await getSubscriptionsByChatId(chatId);
  if (!s) return;
  const newSubscriptions = s.subscriptions.filter((s) => s !== lastName);
  if (newSubscriptions.length === 0) {
    return removeSubscriptions(chatId);
  }
  await pool.query(sql`
    UPDATE subscriptions s
    SET subscriptions = ${sql.json(newSubscriptions)}
    WHERE s.chat_id = ${chatId}
  `);
}
