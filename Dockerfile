FROM node:15-alpine

WORKDIR /app

COPY . ./

CMD ["sh", "-c", "yarn start"]
