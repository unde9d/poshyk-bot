--subscriptions (up)
CREATE TABLE subscriptions
(
    chat_id INT NOT NULL,
    subscriptions jsonb NOT NULL,
    PRIMARY KEY(chat_id)
);
